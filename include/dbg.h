//
// Created by kuba on 15.07.16.
//

#ifndef SEQ_DBG_H

#define SEQ_DBG_H

#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "timestamp.h"

//#define NDEBUG

#ifdef NDEBUG
#define debug(M, ...)
#define debug_printf(M, ...)
#else
#define debug(M, ...) fprintf(stderr, "DEBUG [%s] %s:%d: " M "\n", readable_timestamp(), __FILE__, __LINE__, ##__VA_ARGS__)
#define debug_printf(M, ...) fprintf(stderr , M, ##__VA_ARGS__)
#endif

#define clean_errno() (errno == 0 ? "None" : strerror(errno))

#define log_err(M, ...) fprintf(stderr, "[ERROR] [%s] (%s:%d: errno: %s) " M "\n", readable_timestamp(), __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)

#define log_warn(M, ...) fprintf(stderr, "[WARN] [%s] (%s:%d: errno: %s) " M "\n", readable_timestamp(), __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)

#define log_info(M, ...) fprintf(stderr, "[INFO] [%s] (%s:%d) " M "\n", readable_timestamp(), __FILE__, __LINE__, ##__VA_ARGS__)

#define check(A, M, ...) if(!(A)) { log_err(M, ##__VA_ARGS__); errno=0; goto error; }

#define sentinel(M, ...)  { log_err(M, ##__VA_ARGS__); errno=0; goto error; }

#define check_mem(A) check((A), "Out of memory.")

#define check_debug(A, M, ...) if(!(A)) { debug(M, ##__VA_ARGS__); errno=0; goto error; }

#endif //SEQ_DBG_H

//
// Created by kuba on 15.09.16.
//

#ifndef SEQ_WORKER_H
#define SEQ_WORKER_H

#include "types.h"

void start_test(dataArray2D data, int n, int k, int sval, int rank, int nodes);

#endif //SEQ_WORKER_H

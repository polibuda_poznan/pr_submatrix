//
// Created by kuba on 16.09.16.
//

#ifndef SUBMATRIX_SAVE_H
#define SUBMATRIX_SAVE_H

#include <time.h>

void init_save_result(int n, int k, int sval, int nodes, char *fname);
void save_result(unsigned long long row, unsigned long long col);
void finish_save_result(unsigned long long count, clock_t time);

#endif //SUBMATRIX_SAVE_H

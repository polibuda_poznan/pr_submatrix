//
// Created by kuba on 15.09.16.
//

#ifndef SEQ_TYPES_H_H
#define SEQ_TYPES_H_H

struct dataArray2D
{
    int **arr;
    unsigned long long size;
} typedef dataArray2D;

struct dataArray
{
    int *arr;
    unsigned long long size;
} typedef dataArray;


#endif //SEQ_TYPES_H_H

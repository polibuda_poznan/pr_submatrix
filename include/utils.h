//
// Created by kuba on 15.07.16.
//

#ifndef SEQ_UTILS_H
#define SEQ_UTILS_H

#include "types.h"


#define MONITOR_RANK 0

#define TAG_DATA_HEAD  5
#define TAG_DATA_BODY  6


dataArray2D array_load_from_file(char *fname);
void array_broadcast(dataArray2D data, int rank, int root);
dataArray2D array_square_init_empty(int num);


int sum_subarray(dataArray2D array, int *rows, int *columns, int k);
FILE *safeOpen(char *fname, char *mode);
void array_dispose(dataArray2D data);

dataArray2D get_k_sized_index_subsets(int maxIndex, int k);

struct progressTresholds
{
    unsigned long over75percent;
    unsigned long over50percent;
    unsigned long over25percent;
} typedef progressTresholds;

int rised_over_treshold(unsigned long value, unsigned long treshold);
progressTresholds calculate_tresholds(unsigned long total);
void display_progress(progressTresholds tresholds, unsigned long current, char *procname, int rank);

#endif //SEQ_UTILS_H

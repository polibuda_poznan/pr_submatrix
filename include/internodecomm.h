//
// Created by kuba on 15.09.16.
//

#ifndef SEQ_INTERNODES_COMM_H
#define SEQ_INTERNODES_COMM_H

#include <mpi.h>

#define MSG_BUF_SIZE    4
#define SAVE_RESULT     1
#define SAVE_END        2

char procname[MPI_MAX_PROCESSOR_NAME];

#endif //SEQ_INTERNODES_COMM_H

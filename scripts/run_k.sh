#!/usr/bin/env bash
mpirun -np 5 ./../submatrix ../input/in16.txt 2 4
mpirun -np 5 ./../submatrix ../input/in16.txt 3 9
mpirun -np 5 ./../submatrix ../input/in16.txt 4 16
mpirun -np 5 ./../submatrix ../input/in16.txt 5 25
mpirun -np 5 ./../submatrix ../input/in16.txt 6 36
mpirun -np 5 ./../submatrix ../input/in16.txt 7 49
mpirun -np 5 ./../submatrix ../input/in16.txt 8 64
mpirun -np 5 ./../submatrix ../input/in16.txt 9 81
mpirun -np 5 ./../submatrix ../input/in16.txt 10 100
mpirun -np 5 ./../submatrix ../input/in16.txt 11 121
mpirun -np 5 ./../submatrix ../input/in16.txt 12 144
mpirun -np 5 ./../submatrix ../input/in16.txt 13 169
mpirun -np 5 ./../submatrix ../input/in16.txt 14 196

#!/usr/bin/env bash
find . -type f \( -name \*.c -o -name \*.h \)  -exec cat {} + | wc -l

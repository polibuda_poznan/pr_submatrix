#!/usr/bin/env bash
mpirun -np 11 ./../submatrix ../input/in8.txt 4 16
mpirun -np 11 ./../submatrix ../input/in10.txt 5 25
mpirun -np 11 ./../submatrix ../input/in12.txt 6 36
mpirun -np 11 ./../submatrix ../input/in14.txt 7 49
mpirun -np 11 ./../submatrix ../input/in16.txt 8 64
mpirun -np 11 ./../submatrix ../input/in17.txt 8 64
mpirun -np 11 ./../submatrix ../input/in18.txt 9 81
mpirun -np 11 ./../submatrix ../input/in19.txt 9 81
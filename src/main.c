#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "../include/dbg.h"
#include "../include/utils.h"
#include "../include/worker.h"
#include "../include/monitor.h"
#include "../include/internodecomm.h"


int main(int argc, char *argv[])
{
    char *default_fname = "/home/kuba/Dokumenty/Studia/6SEM_lin/PR/projekt/seq/in.txt";
    char *fname;

    int nodes, rank;
    int name_len;
    unsigned long long solutions_count;
    dataArray2D data;

    fname = argc > 1 ? argv[1] : default_fname;

    if(argc < 4)
    {
        fprintf(stderr, "USAGE: %s <input file> <K> <sval>\n", argv[0]);
        return 0;
    }

    MPI_Init(NULL, NULL);

    MPI_Comm_size(MPI_COMM_WORLD, &nodes);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name( procname, &name_len );

    int n, k, sval;

    MPI_Barrier(MPI_COMM_WORLD);
    if(rank == MONITOR_RANK) {
        data = array_load_from_file(fname);

        n = data.size;
        k = atoi(argv[2]);
        sval = atoi(argv[3]);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Bcast(&n, 1, MPI_INT, MONITOR_RANK, MPI_COMM_WORLD);
    MPI_Bcast(&k, 1, MPI_INT, MONITOR_RANK, MPI_COMM_WORLD);
    MPI_Bcast(&sval, 1, MPI_INT, MONITOR_RANK, MPI_COMM_WORLD);

    if(rank != MONITOR_RANK) {
        data = array_square_init_empty(n);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    array_broadcast(data, rank, MONITOR_RANK);

    if(rank == MONITOR_RANK) {
        solutions_count = start_monitor(n, k, sval, nodes);
        debug("[%s][MONITOR] Success# = %llu", procname, solutions_count);
    }
    else
    {
        start_test(data, n, k, sval, rank, nodes);
        debug("[%s][NODE %d] 100%% done, leaving", procname, rank);
    }

    MPI_Finalize();
    array_dispose(data);

    return 0;
}

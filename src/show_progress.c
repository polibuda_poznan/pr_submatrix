//
// Created by kuba on 13.09.16.
//
#include <time.h>
#include "../include/dbg.h"
#include "../include/utils.h"

int rised_over_treshold(unsigned long value, unsigned long treshold)
{
    if(value >= treshold && (value - 1) < treshold)
    {
        return 1;
    }
    return 0;
}

progressTresholds calculate_tresholds(unsigned long total)
{
    progressTresholds tresholds;
    tresholds.over25percent = total / 4;
    tresholds.over50percent = total / 2;
    tresholds.over75percent = tresholds.over25percent * 3;

    return tresholds;
}

void display_progress(progressTresholds tresholds, unsigned long current, char *procname, int rank)
{
    if(rised_over_treshold(current, tresholds.over75percent))
    {
        debug("[%s][NODE %d] 75%% done", procname, rank);
    }
    else if(rised_over_treshold(current, tresholds.over50percent))
    {
        debug("[%s][NODE %d] 50%% done", procname, rank);
    }
    else if(rised_over_treshold(current, tresholds.over25percent))
    {
        debug("[%s][NODE %d] 25%% done", procname, rank);
    }
}

//
// Created by kuba on 15.09.16.
//
#include "../include/worker.h"
#include "../include/dbg.h"
#include "../include/internodecomm.h"
#include "../include/utils.h"


void send_result_to_monitor(unsigned long long rowIndex, unsigned long long colIndex)
{
#ifndef MPI_VERSION
    debug("Success: [%d][%d]", rowIndex, colIndex);
#else
    int rank;
    int msg[MSG_BUF_SIZE];
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    msg[0] = rowIndex;
    msg[1] = colIndex;
    msg[2] = SAVE_RESULT;

     MPI_Send(&msg, MSG_BUF_SIZE, MPI_INT, MONITOR_RANK, TAG_DATA_HEAD, MPI_COMM_WORLD);
#endif
}

void end_sending_results()
{
    int msg[MSG_BUF_SIZE];
    msg[2] = SAVE_END;

    MPI_Send(&msg, MSG_BUF_SIZE, MPI_INT, MONITOR_RANK, TAG_DATA_HEAD, MPI_COMM_WORLD);
}

unsigned long long calc_sid(int n, int row, int col)
{
    unsigned long long res = n * row + col;

    return res;
}

int can_mpi_thread_run(int rank, int nodes, unsigned long long  sid, int tidmax)
{
#ifndef MPI_VERSION
    return 1;
#else
    unsigned long long mod = sid % nodes;

    return mod == rank;
#endif
}

void start_test(dataArray2D data, int n, int k, int sval, int rank, int nodes)
{
    // generate  all k sized combinations of n size set - all possible indexes for subarrays
    // for NxN array this set stays the same for rows and columns
    unsigned long long row, col, successCount = 0;
    dataArray2D combinations = get_k_sized_index_subsets(n, k);

    debug("[%s][NODE %d] of %d worker nodes, starting checking %llu x %llu combinations",
          procname, rank, nodes - 1, combinations.size, combinations.size);
    progressTresholds tresholds = calculate_tresholds(combinations.size);

    // check all possible subarrays - use every possible rows and columns combinations combination
    for(row = 0; row < combinations.size; ++row)
    {
        for(col = 0; col < combinations.size; ++col)
        {
            int sid = calc_sid(n, row, col);
            if(can_mpi_thread_run(rank - 1, nodes - 1, sid, row * col)) // -1: monitor is not worker
            {
                int sum = sum_subarray(data, combinations.arr[row], combinations.arr[col], k);

                if (sum == sval) {
                    send_result_to_monitor(row, col);
                    successCount++;
                }
            }
        }
        display_progress(tresholds, row, procname, rank);
    }
    end_sending_results();
    array_dispose(combinations);
}

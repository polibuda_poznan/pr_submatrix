//
// Created by kuba on 15.07.16.
//
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "../include/dbg.h"
#include "../include/utils.h"


FILE *safeOpen(char *fname, char *mode)
{
    FILE *ifd = fopen(fname, mode);

    if (ifd == NULL) {
        perror("Nie znaleziono podanego pliku");
        exit(1);
    }

    return ifd;
}


dataArray2D array_load_from_file(char *fname)
{
    int num;
    int row, col;

    dataArray2D result;
    FILE *ifd = safeOpen(fname, "r");

    fscanf(ifd, "%d\n", &num);
    debug_printf("Read N=%d\n", num);
    result = array_square_init_empty(num);

    for(row = 0; row < num; ++row) {

        int cur_val = 0;

        for (col = 0; col < num - 1; ++col) {

            if (!fscanf(ifd, "%d, ", &cur_val)) {
                perror("Value not loadaed properly");
            }

            result.arr[row][col] = cur_val;
            debug_printf("%d ", cur_val);
        }

        if(!fscanf(ifd, "%d\n", &cur_val)) {
            perror("not loadaed properly");
        }
        result.arr[row][col] = cur_val;
        debug_printf("%d\n", cur_val);
    }
    fclose(ifd);

    return result;
}

dataArray2D array_load_from_file_or_broadcast(char *fname, int rank)
{
    int num;

    dataArray2D result;

    if (rank == MONITOR_RANK) {
        result = array_load_from_file(fname);
        num = result.size;

        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Bcast(&num, 1, MPI_INT, MONITOR_RANK, MPI_COMM_WORLD);
    }
    else
    {
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Bcast(&num, 1, MPI_INT, MONITOR_RANK, MPI_COMM_WORLD);

        result = array_square_init_empty(num);
    }

    array_broadcast(result, rank, MONITOR_RANK);

    return result;
}

dataArray2D array_square_init_empty(int num)
{
    int row;

    dataArray2D result;
    result.size = num;

    int **MM = malloc(sizeof(int*) * num);

    for(row = 0; row < num; ++row) {
        MM[row] = malloc(sizeof(int) * num);
    }
    result.arr = MM;

    return result;
}

void array_broadcast(dataArray2D data, int rank, int root)
{
    int row, col, cur_val;

    for (row = 0; row < data.size; ++row) {
        for (col = 0; col < data.size; ++col) {
            if (rank == root)
            {
                cur_val = data.arr[row][col];
            }
            MPI_Bcast(&cur_val, 1, MPI_INT, MONITOR_RANK, MPI_COMM_WORLD);
            data.arr[row][col] = cur_val;
        }
    }
}


void array_dispose(dataArray2D data)
{
    int row;
    // Clean up
    for(row = 0; row < data.size; ++row) {
        free(data.arr[row]);
    }
    free(data.arr);
}

void print_combination_set(dataArray2D combinations, int n, int k)
{
#ifdef NDEBUG
    return;
#else
    int row, col;
    for(row = 0; row < combinations.size; ++row)
    {
        for(col = 0; col < k; ++col)
        {
            fprintf(stderr, "[%d]", combinations.arr[row][col]);
        }
        fprintf(stderr, "\n");
    }
#endif
}

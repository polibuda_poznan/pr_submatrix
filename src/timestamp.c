#include <stdio.h>
#include <time.h>

#include "../include/timestamp.h"

#define TIMESTAMP_SIZE 20

char timestamp[TIMESTAMP_SIZE];

char * readable_timestamp(void)
{
    struct tm *gm_now;

    time_t now = time (0);
    gm_now = gmtime (&now);

    strftime (timestamp, TIMESTAMP_SIZE, "%Y-%m-%d %H:%M:%S", gm_now);

    return timestamp;
}
//
// Created by kuba on 14.08.16.
//
#include <stdio.h>

#include "../include/save.h"
#include "../include/types.h"

#include "../include/utils.h"

static FILE *ifd = 0;
static dataArray2D indexCombinations;
static int _k;


void init_save_result(int n, int k, int sval, int nodes, char *fname)
{
    ifd = safeOpen(fname, "a+");
    _k = k;
    indexCombinations = get_k_sized_index_subsets(n, k);

    fprintf(ifd, "N: %d  k: %d sval = %d, %d nodes \n", n, k, sval, nodes);
}

void save_result(unsigned long long row, unsigned long long col)
{
    unsigned long long i;
    fprintf(ifd, "rows = [ ");
    for(i = 0; i < _k; ++i)
    {
        fprintf(ifd, "[%d]", indexCombinations.arr[row][i]);
    }
    fprintf(ifd, " ], columns = [ ");
    for(i = 0; i < _k; ++i)
    {
        fprintf(ifd, "[%d]", indexCombinations.arr[col][i]);
    }
    fprintf(ifd, " ]\n");
}

void finish_save_result(unsigned long long count, clock_t time)
{
    int msecTotal = time * 1000 / CLOCKS_PER_SEC;
    int msec = msecTotal % 1000;
    int secTotal = msecTotal / 1000;
    int sec = secTotal % 60;
    int minTotal = secTotal / 60;
    int min = minTotal % 60;
    int hr = minTotal / 60;

    fprintf(ifd, "%llu solutions in %d,%ds [%d:%d %d.%d]\n", count, secTotal, msec, hr, min, sec, msec);
    array_dispose(indexCombinations);
    fclose(ifd);
}


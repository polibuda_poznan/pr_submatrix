#include <assert.h>
#include <math.h>
#include <stdint.h>

#include "../include/dbg.h"
//
// Created by Kuba on 15.07.16.
//

#define FAC_MAX_ARGUMENT 21
unsigned long long Factorial(int n)
{
    // using builtin types we can only calculate factorials up to 20
    // so why don't use precalculated lookup array anyway, huh?
    unsigned long long precalc[FAC_MAX_ARGUMENT] = {
            1,
            1,
            2,
            6,
            24,
            120,
            720,
            5040,
            40320,
            362880,
            3628800,
            39916800,
            479001600,
            6227020800,
            87178291200,
            1307674368000,
            20922789888000,
            355687428096000,
            6402373705728000,
            121645100408832000,
            2432902008176640000
    };
    if(n <= FAC_MAX_ARGUMENT) {
        return precalc[n];
    }
    return  -1;
}

unsigned long long NewtonSymbol(int n, int k)
{
    unsigned long long nn = n > k? n : k;
    unsigned long long kk = n > k? k : n;

    if(kk == 0) { return 0; }

    unsigned long long l = Factorial(nn);
    unsigned long long m = Factorial(kk) * Factorial(nn - kk);
    unsigned long long result = l / m;

    return result;
}
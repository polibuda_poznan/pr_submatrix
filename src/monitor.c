//
// Created by kuba on 15.09.16.
//
#include <stdio.h>
#include <time.h>

#include "../include/dbg.h"
#include "../include/internodecomm.h"
#include "../include/math.h"
#include "../include/monitor.h"
#include "../include/save.h"
#include "../include/utils.h"

int start_monitor(int n, int k, int sval, int nodes)
{
    int end_tokens = 0;
    int solutions_count = 0;
    clock_t start, diff;
    unsigned long long combination_set_count = NewtonSymbol(n, k);

    start = clock();

    debug("[%s][MONITOR] Array %d x %d, k = %d, Sval = %d",
          procname, n, n, k, sval);
    debug("[%s][MONITOR] %d nodes will check at most %llu hits",
          procname, nodes - 1, combination_set_count * combination_set_count);

    init_save_result(n, k, sval, nodes - 1, "result.txt");

    while(end_tokens < nodes - 1)
    {
        int msg[MSG_BUF_SIZE];

        MPI_Recv(&msg, MSG_BUF_SIZE, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        if(msg[2] == SAVE_END)
        {
            end_tokens++;
            debug("[%s][MONITOR] %d of %d workers finished", procname, end_tokens, nodes - 1);
        }
        else
        {
            solutions_count++;
            int row = msg[0];
            int col = msg[1];
            // debug("[%s][MONITOR] %d success at [%d][%d]", procname, solutions_count, row, col);
            // save_result(row, col);
        }
    }

    dataArray2D combinations = get_k_sized_index_subsets(n, k);
    array_dispose(combinations);

    diff = clock() - start;
    int msec = diff * 1000 / CLOCKS_PER_SEC;
    debug("[%s][MONITOR] Time [%d:%d]", procname, msec / 1000, msec % 1000);
    finish_save_result(solutions_count, diff);

    return solutions_count;
}



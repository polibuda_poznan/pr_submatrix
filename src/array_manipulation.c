//
// Created by kuba on 16.07.16.
//
#include <stdlib.h>
#include "../include/dbg.h"
#include "../include/math.h"
#include "../include/utils.h"

// Create set of all k sized subsets (combinations) of indices from 0 to n
static void generate_k_sized_index_subsets(int n, int k, int current_i, dataArray *current_subset, int current_k, dataArray2D *solution)
{
    int col, cur_s_size;
    if (current_subset == 0)
    {
        current_subset = malloc(sizeof(dataArray));
        current_subset->arr = malloc(sizeof(int) * k);
        current_subset->size = 0;
    }
    cur_s_size = current_subset->size;
    // successful stop clause
    if (cur_s_size >= k)
    {
        int i = 0;
        for(col = 0; col < k; ++col)
        {
            solution->arr[solution->size][col] = current_subset->arr[col];
        }
        ++solution->size;
        return;
    }
    //unsuccessful stop
    if (current_i == n)
    {
        return;
    }
    // continue recursive generation:
    // branch where x is not in the subset
    generate_k_sized_index_subsets(n, k, current_i+1, current_subset, current_k, solution);

    current_subset->arr[cur_s_size] = current_i;
    current_subset->size = cur_s_size + 1;
    // branch where x is in the subset
    generate_k_sized_index_subsets(n, k, current_i+1, current_subset, current_k + 1, solution);
}



dataArray2D get_k_sized_index_subsets(int n, int k)
{
    dataArray2D res;

    dataArray *current_subset;
    long row;
    long combination_set_count = NewtonSymbol(n, k);

    int **MM = malloc(sizeof(int*) * combination_set_count);
    for(row = 0; row < combination_set_count; ++row)
    {
        MM[row] = malloc(sizeof(int) * k);
        if(MM[row] == NULL)
        {
            debug("Failed alocation for row %ld", row);
            perror("Failed to allocate");
        }
    }

    current_subset = malloc(sizeof(dataArray));
    if(current_subset == NULL)
    {
        debug("Failed alocation for row %ld", row);
        perror("Failed to allocate");
    }
    current_subset->arr = malloc(sizeof(int) * k);
    current_subset->size = 0;

    res.arr = MM;
    res.size = 0;

    generate_k_sized_index_subsets(n, k, 0, current_subset, 0, &res);

    free(current_subset->arr);
    free(current_subset);

    return res;
}


// res[n x k]
void fill(dataArray2D *res, int n, int k)
{
    int row, col;
    for(row = 0; row < n; ++row)
    {
        int col;
        for(col = 0; col < k; ++col)
        {
            res->arr[row][col] = row;
            ++res->size;
        }
    }
}

int sum_subarray(dataArray2D array, int *rows, int *columns, int k)
{
    int row, col, i, j;
    int sum = 0;

    for(i = 0; i < k; ++i)
    {
        row = rows[i];

        for(j = 0; j < k; ++j)
        {
            col = columns[j];

            sum += array.arr[row][col];
        }
    }

    return sum;
}